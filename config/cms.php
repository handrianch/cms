<?php

return [
    'image' => [
        'directory' => storage_path('app\\public\\img\\'),
        'size' => [
            'width' => 250,
            'height' => 170,
        ],
    ],
    'pagination' => [
        'limit' => 8,
    ],
    'category' => [
        'default' => 6,
    ],
    'user' => [
        'default' => 1,
    ]
];
