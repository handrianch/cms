@extends('layouts.error.master')

@section('content')
 <h2>Page Not Found</h2>
 <p>Sorry, the page you're looking for couldn't be found</p>
@endsection