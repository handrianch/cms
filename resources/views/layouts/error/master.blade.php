<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <title>MyBlog | My Awesome Blog</title>

    <link href='https://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css">
    <link rel="stylesheet" href="{{ asset("css/bootstrap.min.css") }}">
    <link rel="stylesheet" href="{{ asset("css/custom.css") }}">
</head>
<body>
    <header>
        @include('layouts.frontend.navbar')
    </header>

    <div class="container">
        <div class="row">
            <div class="col-md-12 error-page">
                @yield('content')
            </div>
        </div>
    </div>

    <footer>
        @include('layouts.frontend.footer')
    </footer>

    <script src="{{ asset("js/bootstrap.min.js") }}"></script>
</body>
</html>