@foreach($postsCount as $key => $value)
    <a href="?show={{ $key }}" class="">
        @if(request('show') == $key)
        <span class="btn btn-danger btn-xs">{{ ucwords($key) }} ({{ $value }})</span>
        @else
        @if(!request()->has('show'))
            @if($key == 'all')
                <span class="btn btn-danger btn-xs">{{ ucwords($key) }} ({{ $value }})</span>
            @else
                {{ ucwords($key) }} ({{ $value }})
            @endif
        @else
            {{ ucwords($key) }} ({{ $value }})
        @endif
        @endif
    </a> |
@endforeach