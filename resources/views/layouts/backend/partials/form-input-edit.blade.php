<div class="col-xs-9">
  <div class="box">
    <div class="box-body ">
      @csrf
      <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
        <label for="title">Title</label>
        <input type="text" class="form-control" name="title" id="title" 
        value="{{ $post->title }}">
        
        @if($errors->has('title'))
          <span class="help-block">
            <strong>{{ $errors->first('title') }}</strong>
          </span>
        @endif

      </div>

      <div class="form-group {{ $errors->has('slug') ? 'has-error' : ''}}">
        <label for="slug">Slug</label>
        <input type="text" class="form-control" name="slug" id="slug" value="{{ $post->slug }}">

        @if($errors->has('slug'))
          <span class="help-block">
            <strong>{{ $errors->first('slug') }}</strong>
          </span>
        @endif

      </div>

      <div class="form-group excerpt {{ $errors->has('excerpt') ? 'has-error' : ''}}">
        <label for="excerpt">Excerpt</label>
        <textarea class="form-control" name="excerpt" id="excerpt">{{ $post->excerpt }}</textarea>

        @if($errors->has('excerpt'))
          <span class="help-block">
            <strong>{{ $errors->first('excerpt') }}</strong>
          </span>
        @endif

      </div>

      <div class="form-group {{ $errors->has('body') ? 'has-error' : ''}}">
        <label for="body">Body</label>
        @if($errors->has('body'))
          <span class="help-block">
            <strong>{{ $errors->first('body') }}</strong>
          </span>
        @endif
        <textarea class="form-control {{ $errors->has('body') ? 'has-error' : ''}}" name="body" id="body">{{ $post->body }}</textarea>
      </div>
    </div>
  </div>
</div>

<div class="col-xs-3">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Publish</h3>
      <br>
      <span class="label label-{{ $post->status()['class'] }}">{{ $post->status()['status'] }}</span>
    </div>
    @if($post->status()['status'] !== 'published')
    <div class="box-body">
      <div class="form-group {{ $errors->has('published_at') ? 'has-error' : ''}}">
          <div class='input-group date' id='datetimepicker'>
            <input type="text" name="published_at" id="publish-date" class="form-control" placeholder="ex : 2018/05/01 09:00:00" value="{{ $post->published_at ? $post->published_at->format('Y/m/d H:i:s') : '' }}">
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
          </div>
        
        @if($errors->has('published_at'))
          <span class="help-block">
            <strong>{{ $errors->first('published_at') }}</strong>
          </span>
        @endif

      </div>
    </div>
    @endif
    <div class="box-footer clearfix">
      <div class="pull-left">
        <a href="#" class="btn btn-default" id="draft">Save Draft</a>
      </div>
      <div class="pull-right">
        <div class="form-group">
            <button class="btn btn-primary" type="submit" id="publish">Publish</button>
          </div>
      </div>
    </div>
  </div>
  <div class="box">
    <div class="box-header">
      <h3 class="box-title with-border">Category</h3>
    </div>
    <div class="box-body text-center">
          <div class="form-group {{ $errors->has('category') ? 'has-error' : ''}}">
            <select name="category" class="form-control">
              <option disabled selected hidden>Choose Category</option>
              @foreach($categories as $key => $value)
                <option value="{{ $value }}" {{ $post->category_id == $value ? 'selected' : '' }}>{{ $key }}</option>
              @endforeach
            </select>

            @if($errors->has('category'))
              <span class="help-block">
                <strong>{{ $errors->first('category') }}</strong>
              </span>
            @endif

          </div>
    </div>
  </div>
  <div class="box">
    <div class="box-header with-border">
      <div class="h3 box-title">Feature Image</div>
    </div>
    <div class="box-body text-center">
      <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
            <div class="fileinput fileinput-new" data-provides="fileinput">
              <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                <img src="{{ $post->imagethumbUrl ? $post->imageThumbUrl : asset('storage/img/noimage.png') }}" alt="...">
              </div>
              <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
              <div>
                <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="image"></span>
                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
              </div>
          </div>

            @if($errors->has('image'))
              <span class="help-block">
                <strong>{{ $errors->first('image') }}</strong>
              </span>
            @endif

          </div>
    </div>
  </div>
</div>