<table class="table table-bordered">
    <thead>
    <tr>
        <th>Action</th>
        <th>Title</th>
        <th>View Posts</th>
        <th>Count</th>
        <th>Date</th>
    </tr>
    </thead>

    <tbody>
    @foreach($categories as $category)
        <tr>
            <td style="width: 7%;">
                <a href="{{ route('category.edit', ['id' => $category->id]) }}" class="btn btn-default btn-xs {{ $category->id === 6 ? "disabled" : ''}}">
                <i class="fa fa-edit"></i>
                </a>
                <form action="{{ route('category.destroy', ['id' => $category->id]) }}" method="POST" style="display: inline-block">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger btn-xs" {{ $category->id === 6 ? "disabled" : ''}} onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></button>
                </form>
            </td>
            <td>
                {{ $category->title }}
            </td>
            <td>
                <a href="{{ route('blog.index') }}?show=category&category={{ $category->slug }}">View Posts</a>
            </td>
            <td class="text-center" style="width: 5%">
                <strong>{{ $category->count }}</strong>
            </td>
            <td style="width: 15%"> {{ $category->dateFormat }} </td>
        </tr>
    @endforeach
    </tbody>
</table>