<script type="text/javascript" src="{{ asset('backend/plugins/simple-mde/js/simplemde.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/datetimepicker/js/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/datetimepicker/js/datetimepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/plugins/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/custom.js') }}"></script>