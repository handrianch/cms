<div class="alert alert-warning">
    <strong>{{ session('success') }}</strong>
    @if(session()->has('trashedPost'))
        <form action="{{ route('blog.restore', ['id' => session('trashedPost')]) }}" method="POST" style="display: inline-block">
            @csrf
            @method('PATCH')
            <button class="btn btn-success btn-xs"><i class="fa fa-undo"></i> Undo</button>
        </form>
    @endif
</div>