<table class="table table-bordered">
    <thead>
    <tr>
        <th>Action</th>
        <th>Title</th>
        <th>Author</th>
        <th>Category</th>
        <th>Date</th>
    </tr>
    </thead>

    <tbody>
    @foreach($posts as $post)
        <tr>
        <td style="width: 7%">
            <a href="{{ route('blog.edit', ['id' => $post->id]) }}" class="btn btn-default btn-xs">
            <i class="fa fa-edit"></i>
            </a>
            <form action="{{ route('blog.destroy', ['id' => $post->id]) }}" method="POST" style="display: inline-block">
            @csrf
            @method('DELETE')
            <button class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
            </form>
        </td>
        <td style="width: 40%"> {{ $post->title }}</td>
        <td width="120"> {{ $post->author->name }}</td>
        <td width="130"> {{ $post->category->title }}</td>
        <td width="160">
            {{ $post->dateFormat() }}
            &nbsp;
            <span class="label label-{{ $post->status()['class'] }}">{{ $post->status()['status'] }}</span>
        </td>
        </tr>
    @endforeach
    </tbody>
</table>