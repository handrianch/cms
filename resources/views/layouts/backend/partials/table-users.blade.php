<table class="table table-bordered">
    <thead>
    <tr>
        <th>Action</th>
        <th>Name</th>
        <th>Email</th>
        <th>Role</th>
    </tr>
    </thead>

    <tbody>
    @foreach($users as $user)
        @if(auth()->id() !== config('cms.user.default') && $user->id === config('cms.user.default'))
            @continue
        @else
            <tr>
                <td style="width: 7%">
                    <a href="{{ route('users.edit', ['id' => $user->id]) }}" class="btn btn-default btn-xs">
                    <i class="fa fa-edit"></i>
                    </a>
                    <form action="{{ route('users.confirm', ['id' => $user->id]) }}" method="POST" style="display: inline-block">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger btn-xs" {{ (int)$user->id === config('cms.user.default') ? 'disabled' : ''}}><i class="fa fa-trash"></i></button>
                    </form>
                </td>
                <td style="width: 40%"> {{ $user->name }}</td>
                <td width="120"> {{ $user->email }}</td>
                <td width="130"> - </td>
            </tr>
        @endif
    @endforeach
    </tbody>
</table>