<div class="col-xs-9">
  <div class="box">
    <div class="box-body ">
      @if(session()->has('error'))
            <div class="alert alert-danger">
              {{ session('error') }} : <strong>{{ session('name') }}</strong>
            </div>
      @endif

      @csrf

      <div class="form-group
      {{ $errors->has('name') ? 'has-error' : null ?? session()->has('error') ? 'has-error' : ''}}
      ">
        <label for="name">Name</label>
        <input type="text" class="form-control" name="name" id="name"
        value="{{ $user->name }}">

        @if($errors->has('name') || session()->has('error'))
          <span class="help-block">
            <strong>{{ $errors->first('name') ?: null ?? session('error') }}</strong>
          </span>
        @endif

      </div>

      <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
        <label for="email">Email</label>
        <input type="email" class="form-control" name="email" id="email" value="{{ $user->email }}">

        @if($errors->has('email'))
          <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
          </span>
        @endif

      </div>

       <div class="form-group {{ $errors->has('bio') ? 'has-error' : ''}}">
        <label for="bio">Bio</label>
        <textarea class="form-control" name="bio" id="bio">{{ $user->bio }}</textarea>

        @if($errors->has('bio'))
          <span class="help-block">
            <strong>{{ $errors->first('bio') }}</strong>
          </span>
        @endif

      </div>

      <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
        <label for="password">New Password</label>
        <input type="password" class="form-control" name="password" id="password" value="{{ old('password') }}">

        @if($errors->has('password'))
          <span class="help-block">
            @if(!(preg_match("/The password confirmation does not match./i", $errors->first('password'))))
              <strong>{{ $errors->first('password') }}</strong>
            @endif
          </span>
        @endif

      </div>

      <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
        <label for="password_confirmation">Password Confirmation</label>
        <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" value="{{ old('password_confirmation') }}">

        @if($errors->has('password'))
          @if(preg_match("/The password confirmation does not match./i", $errors->first('password')))
          <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
          </span>
          @endif
        @endif

      </div>

      <div class="form-group">
          <button class="btn btn-primary" type="submit" id="publish">Edit User</button>
          <a href="{{ route("users.index") }}" class="btn btn-default">Cancel</a>
      </div>
    </div>
  </div>
</div>