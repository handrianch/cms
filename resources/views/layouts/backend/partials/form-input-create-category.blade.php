<div class="col-xs-9">
  <div class="box">
    <div class="box-body">
      <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
        <label for="title">Title</label>
        <input type="text" class="form-control" name="title" id="title"
        value="{{ $category->title ?? old('title') }}">

        @if($errors->has('title'))
          <span class="help-block">
            <strong>{{ $errors->first('title') }}</strong>
          </span>
        @endif

      </div>

      <div class="form-group {{ $errors->has('slug') ? 'has-error' : ''}}">
        <label for="slug">Slug</label>
        <input type="text" class="form-control" name="slug" id="slug" value="{{ $category->slug ?? old('slug') }}">

        @if($errors->has('slug'))
          <span class="help-block">
            <strong>{{ $errors->first('slug') }}</strong>
          </span>
        @endif

      </div>

      <div class="form-group">
        <button class="btn btn-primary" type="submit" id="publish">Save</button>
        <a href="{{ route('category.index') }}" class="btn btn-default" id="draft">Cancel</a>
      </div>
    </div>
  </div>
</div>