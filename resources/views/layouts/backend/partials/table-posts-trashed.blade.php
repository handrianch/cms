<table class="table table-bordered">
    <thead>
    <tr>
        <th>Action</th>
        <th>Title</th>
        <th>Author</th>
        <th>Category</th>
        <th>Date</th>
    </tr>
    </thead>

    <tbody>
    @foreach($posts as $post)
        <tr>
        <td style="width: 7%">
            <form action="{{ route('blog.restore', ['id' => $post->id]) }}" method="POST" style="display: inline-block">
                @csrf
                @method('PATCH')
                <button type="submit" class="btn btn-default btn-xs"><i class="fa fa-undo" title="Restore Post" style="outline: none"></i></button>
            </form>
            <form action="{{ route('blog.delete', ['id' => $post->id]) }}" method="POST" style="display: inline-block">
                @csrf
                @method('DELETE')
                <button title="Delete Permanently" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure to delete this posts permanently?')" style="outline: none"><i class="fa fa-times"></i></button>
            </form>
        </td>
        <td style="width: 40%"> {{ $post->title }}</td>
        <td width="120"> {{ $post->author->name }}</td>
        <td width="130"> {{ $post->category->title }}</td>
        <td width="160">
            {{ $post->dateFormat() }}
            &nbsp;
            <span class="label label-{{ $post->status()['class'] }}">{{ $post->status()['status'] }}</span>

            <span class="label label-warning">Trashed</span>
        </td>
        </tr>
    @endforeach
    </tbody>
</table>