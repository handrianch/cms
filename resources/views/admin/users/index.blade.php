@extends('layouts.backend.master')

@section('title')
  All Users | {{ explode(" ", auth()->user()->name)[1] }}
@endsection

@section('content')
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Users
      <small>Display all users</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('users.index') }}">Users</a></li>
        <li class="active">All Users</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header clearfix">
                <div>
                  <a href="{{ route('users.create') }}" class="btn btn-success">Add New</a>
                </div>
              </div>
              <div class="box-body ">
                @if(session()->has('success'))
                  @include('layouts.backend.partials.success-message')
                @elseif(session()->has('errors'))
                  @include('layouts.backend.partials.errors-message')
                @endif
                @if(!$users->count())
                  <div class="alert alert-danger">
                    <strong>No record found</strong>
                  </div>
                @else
                  @include('layouts.backend.partials.table-users')
                @endif
              </div>
              <!-- /.box-body -->
              <div class="box-footer clear-fix">
                <div class="pull-left">
                  @if($users->count() > 0)
                    {{ $users->links() }}
                  @endif
                </div>
                <div class="pull-right">
                  <small>{{ $count }} {{ str_plural('items', $count ) }}</small>
                </div>
              </div>
            </div>
          </div>
        </div>
    </section>
@endsection