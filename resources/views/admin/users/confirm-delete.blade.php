@extends('layouts.backend.master')

@section('title')
    Confirm Delete User | {{ explode(" ", auth()->user()->name)[1] }}
@endsection

@section('content')

    <section class="content-header">
      <h1>
        User
      <small>Confirm Delete User</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('users.index') }}">User</a></li>
        <li class="active">Delete User</li>
      </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <p>You have specified this user for deletion :</p>
                        <p><strong>ID #{{ $user->id }}</strong> {{ $user->name }}</p>
                        <p>What should be done with content owns by this user?</p>

                        <form action="{{ route('users.destroy', ['id' => $user->id]) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <div class="form-group">
                                <input type="radio" name="action" checked value="delete"> Delete All
                                {{ str_plural('Content', $postCount)}} ({{ $postCount }})
                            </div>
                            <div class="form-group">
                                <input type="radio" name="action" value="move"> Attribute Content To &nbsp;
                                <select name="username" id="username">
                                    @foreach($users as $key => $value)
                                        @if((int)$value !== (int)$user->id)
                                            <option value="{{ $value }}">{{ $key }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-danger">Confirm Deletion</button>
                                <a href="{{ route('users.index') }}" class="btn btn-default">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection