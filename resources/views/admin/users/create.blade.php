@extends('layouts.backend.master')

@section('title')
  Create User | {{ explode(" ", auth()->user()->name)[1] }}
@endsection

@section('content')
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User
      <small>Create User</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('users.index') }}">User</a></li>
        <li class="active">Create User</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
          <form action="{{ route('users.store') }}" method="POST" id="form-data">
            @include('layouts.backend.partials.form-user-create')
          </form>
        </div>
    </section>
@endsection