@extends('layouts.backend.master')

@section('title')
  Edit User | {{ explode(" ", auth()->user()->name)[1] }}
@endsection

@section('style')
  <link rel="stylesheet" href="{{ asset('/backend/plugins/simple-mde/css/simplemde.min.css') }}">
@endsection

@section('content')
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User
      <small>Edit User</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('users.index') }}">Users</a></li>
        <li class="active">Edit User</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
          <form action="{{ route('users.update', ['users' => $user->id ]) }}" method="POST" id="form-data">
            @method('PATCH')
            @include('layouts.backend.partials.form-user-edit')
          </form>
        </div>
    </section>
@endsection

@section('script')
  <script src={{ asset('/backend/plugins/simple-mde/js/simplemde.min.js') }}></script>
  <script>
    let bio = new SimpleMDE({ element: $('#bio')[0] })
  </script>
@endsection