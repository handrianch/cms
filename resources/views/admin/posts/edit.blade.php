@extends('layouts.backend.master')

@section('title')
  Edit Post | {{ explode(" ", auth()->user()->name)[1] }}
@endsection

@section('style')
  @include('layouts.backend.partials.form-styling')
@endsection

@section('content')
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Blog
      <small>Edit Post</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('blog.index') }}">Blog</a></li>
        <li class="active">Edit Post</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
          <form action="{{ route('blog.update', ['blog' => $post->id]) }}" method="POST" enctype="multipart/form-data" id="form-data">
            @method('PUT')
            @include('layouts.backend.partials.form-input-edit')
          </form>
        </div>
    </section>
@endsection

@section('script')
  @include('layouts.backend.partials.form-script')
@endsection