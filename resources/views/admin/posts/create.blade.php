@extends('layouts.backend.master')

@section('title')
  Create Post | {{ explode(" ", auth()->user()->name)[1] }}
@endsection

@section('style')
  @include('layouts.backend.partials.form-styling')
@endsection

@section('content')
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Blog
      <small>Create Post</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('blog.index') }}">Blog</a></li>
        <li class="active">Create Post</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
          <form action="{{ route('blog.store') }}" method="POST" enctype="multipart/form-data" id="form-data">
            @include('layouts.backend.partials.form-input-create')
          </form>
        </div>
    </section>
@endsection

@section('script')
  @include('layouts.backend.partials.form-script')
@endsection