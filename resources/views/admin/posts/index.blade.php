@extends('layouts.backend.master')

@section('title')
  All Post | {{ explode(" ", auth()->user()->name)[1] }}
@endsection

@section('content')
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Blog
      <small>Display all blog posts</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('blog.index') }}">Blog</a></li>
        <li class="active">All Posts</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header clearfix">
                @if(strtolower(request()->query('show')) !== 'category')
                <div class="pull-left">
                  <a href="{{ route('blog.create') }}" class="btn btn-success">Add New</a>
                </div>
                <div class="pull-right" style="padding: 7px 0;">
                  @include('layouts.backend.partials.index-action-menu')
                </div>
                @endif
              </div>
              <div class="box-body ">
                @include('layouts.backend.partials.success-message')
                @if(!$posts->count())
                  <div class="alert alert-danger">
                    <strong>No record found</strong>
                  </div>
                @else
                  @if(!$trashed)
                    @include('layouts.backend.partials.table-posts')
                  @else
                    @include('layouts.backend.partials.table-posts-trashed')
                  @endif
                @endif
              </div>
              <!-- /.box-body -->
              <div class="box-footer clear-fix">
                <div class="pull-left">
                  @if($posts->count() > 0)
                    {{ $posts->appends([
                      'show' => request()->query('show'),
                      'category' => request()->query('category')
                      ])->links() }}
                  @endif
                </div>
                <div class="pull-right">
                  <small>{{ $count }} {{ str_plural('items', $count ) }}</small>
                </div>
              </div>
            </div>
          </div>
        </div>
    </section>
@endsection