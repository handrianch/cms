@extends('layouts.backend.master')

@section('title')
  All Categories | {{ explode(" ", auth()->user()->name)[1] }}
@endsection

@section('content')
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Categories
      <small>Display all categories</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('category.index') }}">Categories</a></li>
        <li class="active">All Categories</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <a href="{{ route('category.create') }}" class="btn btn-success">Add New</a>
              </div>
              <div class="box-body ">
                @include('layouts.backend.partials.success-message')
                @if(!$categories->count())
                  <div class="alert alert-danger">
                    <strong>No record found</strong>
                  </div>
                @else
                  @include('layouts.backend.partials.table-categories')
                @endif
              </div>
              <!-- /.box-body -->
              <div class="box-footer clear-fix">
                <div class="pull-left">
                  {{ $categories->links() }}
                </div>
                <div class="pull-right">
                  <small>{{ $count }} {{ str_plural('items', $count ) }}</small>
                </div>
              </div>
            </div>
          </div>
        </div>
    </section>
@endsection