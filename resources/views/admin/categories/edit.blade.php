@extends('layouts.backend.master')

@section('title')
  Edit Category | {{ explode(" ", auth()->user()->name)[1] }}
@endsection

@section('content')
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Category
      <small>Category</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('category.index') }}">Category</a></li>
        <li class="active">Edit Category</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
          <form action="{{ route('category.update', ['category' => $category->id]) }}" method="POST" id="form-data">
            @csrf
            @method('PATCH')
            @include('layouts.backend.partials.form-input-create-category')
          </form>
        </div>
    </section>
@endsection

@section('script')
    <script>
      $('#title').on('input', function(e){
        let title = $(this).val().toLowerCase();
        slug = title.replace(/^&/g, 'and-')
                    .replace(/&/g, '-and-')
                    .replace(/[^0-9a-z-]/g, '-')
                    .replace(/-\-+/g, '-')
                    .replace(/^-+|-+$/g, '');

        $('#slug').val(slug);
      });
    </script>
@endsection