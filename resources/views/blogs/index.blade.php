@extends('layouts.frontend.master')

@section('content')
    @if(!$posts->count())
        <div class="alert alert-warning">
            <p>Nothing Found</p>
        </div>
    @else
        @if(isset($categoryName) || isset($authorName))
            <div class="alert alert-info">
                <p>
                    @if(isset($categoryName))
                        Category : 
                    @elseif(isset($authorName))
                        Author :
                    @endif
                    <strong>{{ $categoryName ?? $authorName }}</strong>
                </p>
            </div>
        @endif

        @foreach($posts as $post)
            <article class="post-item">
                @img($post)
                    <div class="post-item-image">
                        <a href="{{ route('show', ['post' => $post->slug]) }}">
                            <img src="{{ $post->imageURL }}" alt="{{ $post->title }}">
                        </a>
                    </div>
                @endimg
                <div class="post-item-body">
                    <div class="padding-10">
                        <h2><a href="{{ route('show', ['post' => $post->slug]) }}">{{ $post->title }}</a></h2>
                        {!! $post->contentExcerpt !!}
                    </div>

                    <div class="post-meta padding-10 clearfix">
                        <div class="pull-left">
                            <ul class="post-meta-group">
                                <li>
                                    <i class="fa fa-user"></i>
                                    <a href="{{ route('authors', ['author' => $post->author->slug]) }}"> 
                                        {{ $post->author->name }}
                                    </a>
                                </li>
                                <li><i class="fa fa-clock-o"></i><time> {{ $post->date }}</time></li>
                                <li><i class="fa fa-folder"></i><a href="{{ route('categories', $post->category->slug) }}"> {{ $post->category->title }}</a></li>
                                <li><i class="fa fa-comments"></i><a href="#">4 Comments</a></li>
                            </ul>
                        </div>
                        <div class="pull-right">
                            <a href="{{ route('show', ['post' => $post->slug]) }}">Continue Reading &raquo;</a>
                        </div>
                    </div>
                </div>
            </article>
        @endforeach
    @endif


      <ul class="pager">
        @if($posts->currentPage() > 1)
            <li class="previous">
                <a href=" {{ $posts->previousPageUrl() }}"><span aria-hidden="true">&larr;</span> Newer</a>
            </li>
        @endif
        @if($posts->hasMorePages())
            <li class="next"><a href="{{ $posts->nextPageUrl() }}">Older <span aria-hidden="true">&rarr;</span></a></li>
        @endif
      </ul>
    </nav>

@endsection