@extends('layouts.frontend.master')

@section('content')

    <article class="post-item post-detail">
        @img($post)
            <div class="post-item-image">
                <img src="{{ $post->imageUrl }}" alt="{{ $post->title }}" title="{{ $post->title }}">
            </div>
        @endimg

        <div class="post-item-body">
            <div class="padding-10">
                <h1>{{ $post->title }}</h1>

                <div class="post-meta no-border">
                    <ul class="post-meta-group">
                        <li><i class="fa fa-user"></i><a href="{{ route('authors', ['author' => $post->author->name]) }}"> {{ $post->author->name }}</a></li>
                        <li><i class="fa fa-clock-o"></i><time> {{ $post->date }}</time></li>
                        <li><i class="fa fa-folder"></i><a href="{{ route('categories', ['category' => $post->category->slug]) }}"> {{ $post->category->title }}</a></li>
                        <li><i class="fa fa-comments"></i><a href="#">4 Comments</a></li>
                    </ul>
                </div>

                <div class="text-justify">
                    {!! $post->ContentBody !!}
                </div>
            </div>
        </div>
    </article>

    <article class="post-author padding-10">
        <div class="media">
          <div class="media-left">
            <a href="{{ route('authors', ['author' => $post->author->slug]) }}">
              <img alt="{{ $post->author->name }}" src="{{ $post->author->avatar() }}"  width="100px" class="media-object">
            </a>
          </div>
          <div class="media-body">
            <h4 class="media-heading"><a href="{{ route('authors', ['author' => $post->author->slug]) }}">{{ $post->author->name }}</a></h4>
            <div class="post-author-count">
              <a href="{{ route('authors', ['author' => $post->author->slug]) }}">
                  <i class="fa fa-clone"></i>
                  {{ $countPost }} {{ str_plural('post', $countPost) }}
              </a>
            </div>
            {!! $post->author->bio() !!}
          </div>
        </div>
    </article>
    
    <article class="post-comments">
        @include('layouts.frontend.comment')
    </article>
@endsection