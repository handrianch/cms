<?php

/**
 * Route for frontend functionality
 */
Route::get('/', 'Frontend\BlogController@index')->name('index');
Route::get('/blogs/{post}', 'Frontend\BlogController@show')->name('show');
Route::get('/blogs/category/{categorySlug}', 'Frontend\BlogController@category')->name('categories');
Route::get('/blogs/author/{author}', 'Frontend\BlogController@author')->name('authors');
Auth::routes();

/**
 * Route for backend functionality
 */
Route::resource('/backend/blog', Backend\BlogController::class);
Route::resource('/backend/category', Backend\CategoryController::class);
Route::resource('/backend/users', Backend\UserController::class);

Route::name('home')->get('/home', 'Backend\DashboardController@index');
Route::name('blog.restore')->patch('/backend/blog/{blog}/restore', 'Backend\BlogController@restore');
Route::name('blog.delete')->delete('/backend/blog/{blog}/delete', 'Backend\BlogController@delete');
Route::name('users.confirm')->delete('/backend/user/{user}/confirm', 'Backend\UserController@confirm');
