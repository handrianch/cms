$(document).ready(function() {
    (function() {
        $date = moment().format('YYYY/MM/DD HH:mm:ss');
        let publish = $('#publish-date');
        if (publish.val() == '') {
            publish.val($date);
        }
    })();

    $("#title").on('input', function() {
        let title = $(this).val().toLowerCase();
        let slug = title.replace(/^&/g, 'and-')
            .replace(/&/g, '-and-')
            .replace(/[^a-z0-9-]/g, '-')
            .replace(/\-\-+/g, '-')
            .replace(/^-+|-+$/g, '');

        $("#slug").val(slug);
    });

    let excerpt = new SimpleMDE({ element: $("#excerpt")[0] });
    let body = new SimpleMDE({ element: $("#body")[0] });

    $('#datetimepicker').datetimepicker({
        format: "YYYY/MM/DD HH:mm:ss",
        showClear: true,
        showClose: true,
    });

    $('#draft').on('click', function(e) {
        e.preventDefault();
        $('#publish-date').val("");
        $('#form-data').submit();
    });

    $('#publish').on('click', function(e) {
        e.preventDefault();

        let publish = $('#publish-date');

        if (publish.val() == '') {
            $date = moment().format('YYYY/MM/DD HH:mm:ss');
            publish.val($date);
        }

        $('#form-data').submit();
    })
})