<?php

namespace App\Console\Commands;

use Illuminate\Foundation\Console\ModelMakeCommand as command;

class ModelMakeCommand extends Command
{
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\Models';
    }
}
