<?php

namespace App\Views\Composers;

use App\Models\Post;
use App\Models\Category;
use Illuminate\View\View;

class SidebarNavigationComposer
{
    public function compose(View $view)
    {
        $categories = $this->composeCategories();

        $popularPosts = $this->composePopularPosts();

        $view->with('categories', $categories)
             ->with('popularPosts', $popularPosts);
    }

    private function composeCategories()
    {
        return Category::with(['posts' => function($query){
                    $query->published();
                }])->orderBy('title', 'asc')->get();
    }

    private function composePopularPosts()
    {
        return Post::published()->popular()->take(3)->get();
    }
}