<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use GrahamCampbell\Markdown\Facades\Markdown;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'slug', 'bio',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    private const PATTERN = '/admin|Admin|Super|super|Super Admin|Super admin|super admin/';

    public function posts()
    {
        return $this->hasMany(Post::class, 'author_id');
    }

    public function bio()
    {
        return Markdown::convertToHtml(e($this->bio));
    }

    public function avatar($img = false)
    {
        /*
        $size = 100;
        $url = "https://www.gravatar.com/avatar/";
        $path = md5(strtolower(trim($this->email)));
        $default = "https://www.goodfreephotos.com/cache/vector-images/tux-penguin-face-vector-art.png";
        $queryString = "?d=" . urlencode($default) . "&s=" . $size;
        $fullURL = $url . $path . $queryString;

        return $url . $path . $queryString;
        */

        // sementara ini offline
        return asset('storage/backend/img/avatar5.png');
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public static function storeRegisterUser($request)
    {
        if (preg_match(self::PATTERN, $request->input('name'))) {
            session()->flash('error', "Sorry you can't create user with this name");
            session()->flash('name', $request->name);
            return redirect()->back()
                            ->withInput($request->except(['password', 'password_confirmation']));
        }
        $data = $request->all();
        $data['slug'] = str_slug($request->name);

        return $data;
    }

    public function updateUserAccount($request)
    {
        if (preg_match(self::PATTERN, $request->input('name')) && $this->id !== config('cms.user.default')) {
            session()->flash('error', "Sorry you can't edit the name with this name");
            session()->flash('name', $request->name);
            return redirect()->back()
                            ->withInput($request->except(['password', 'password_confirmation']));
        }
        $oldPassword = $this->password;
        $data = $request->all();
        $data['slug'] = str_slug($request->name);
        $this->update($data);

        if ((int)auth()->id() === (int)$this->id) {
            if ($request->has('password') && !(password_verify($oldPassword, $request->password))) {
                auth()->logout();
                return redirect()->route('login');
            }
        }

        return true;
    }

    public function deleteUser($request)
    {
        $action = strtolower($request->input('action'));
        $posts  = Post::withTrashed()->where('author_id', $this->id);

        if ($action === 'delete') {
            $posts->delete();
        } else if ($action === 'move') {
            $posts->update(['author_id' => $request->input('username')]);
        }

        $this->delete();

        return true;
    }
}
