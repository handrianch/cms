<?php

namespace App\Models;

use Carbon\Carbon;
use Intervention\Image\Facades\Image;
use Illuminate\Database\Eloquent\Model;
use GrahamCampbell\Markdown\Facades\Markdown;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'author_id', 'title', 'slug', 'excerpt', 'body', 'published_at', 'category_id', 'image'
    ];

    protected $dates = [
        'published_at', 'deleted_at'
    ];

    private $dirImgAsset;
    private $dirImg;

    public function __construct()
    {
        parent::__construct();

        $this->dirImg = config('cms.image.directory');
        $this->dirImgAsset = 'storage/img/';
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function getImageUrlAttribute($value)
    {
        return asset($this->dirImgAsset . $this->image);
    }

    public function getImageThumbUrlAttribute($value)
    {
        $fullname = $this->makeImgThumbName($this->image);
        $file = $this->dirImgAsset . $fullname;

        if (file_exists($file)) {
            return asset($file);
        }
        return null;
    }

    public function getDateAttribute($value)
    {
        return $this->published_at->toFormattedDateString();
    }

    public function getContentBodyAttribute($value)
    {
        return Markdown::convertToHtml(e($this->body));
    }

    public function getContentExcerptAttribute($value)
    {
        return Markdown::convertToHtml(e($this->excerpt));
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = $value ?? str_slug($this->title);
    }

    public function setPublishedAtAttribute($value)
    {
        $this->attributes['published_at'] = $value ? str_replace('/', '-', $value) : null;
    }

    public function setExcerptAttribute($value)
    {
        $this->attributes['excerpt'] = $value ?? mb_substr($this->body, 0, 200);
    }

    public function scopePublished($query)
    {
        return $query->whereNotNull('published_at')
                     ->where('published_at', '<=', Carbon::now());
    }

    public function scopeScheduled($query)
    {
        return $query->where('published_at', '>', Carbon::now())
                     ->orderBy('published_at', 'desc');
    }

    public function scopeAllPosts($query)
    {
        return $query->withTrashed();
    }

    public function scopeDrafted($query)
    {
        return $query->whereNull('published_at')
                     ->latest();
    }

    public function scopeByCategory($query, $category)
    {
        try {
            $posts = $query->withTrashed()
                           ->with('category')
                           ->get()
                           ->pluck('category')
                           ->where('slug', $category)
                           ->first()
                           ->posts()
                           ->withTrashed();
        } catch (\Error $e) {
            $posts = collect([]);
        }

        return $posts;
    }

    public function scopePopular($query)
    {
        return $query->orderBy('view_count', 'desc');
    }

    public function scopePublish($query, $request)
    {
        $this->author_id = auth()->user()->id;
        $this->title = $request->input('title');
        $this->slug = $request->input('slug');
        $this->body = $request->input('body');
        $this->excerpt = $request->input('excerpt');
        $this->published_at = $request->input('published_at') ?? null;
        $this->category_id = $request->category;
        $this->image = $request->image ?? null;
        $this->save();

        return $this;
    }

    public static function uploadImage($query, $image)
    {
        // store file to disk
        $storeFile = $image->store('img', 'public');
        // remove img/ in file name
        $fullname = mb_substr($storeFile, mb_strpos($storeFile, '/') + 1);

        static::createThumbnail($fullname);

        return $fullname;
    }

    public function dateFormat($time = false)
    {
        $format = 'd/m/Y';
        if ($time) {
            return $this->created_at->format($format) . ' On ' . $this->created_at->format('H:i:s');
        }

        return $this->created_at->format($format);
    }

    public function status()
    {
        $data = [
            'class' => null,
            'status' => null,
        ];
        if (is_null($this->published_at)) {
            $data['class'] = 'success';
            $data['status'] = 'draft';
        } elseif ($this->published_at > Carbon::now()) {
            $data['class'] = 'info';
            $data['status'] = 'schedule';
        } else {
            $data['class'] = 'danger';
            $data['status'] = 'published';
        }

        return $data;
    }

    public function createThumbnail($fullname)
    {
        // concat filename and extension and add postfix _thumb in filename before extension
        $thumbnail = $this->makeImgThumbName($fullname);

        // fetch setting from cms config
        $directory = $this->dirImg;
        $height = config('cms.image.size.height');
        $width = config('cms.image.size.width');

        // make thumbnail
        Image::make($directory . $fullname)
                ->resize($width, $height)
                ->save($directory . $thumbnail);
    }

    public function postUpdate($request)
    {
        $data = [
            'title' => $request->input('title'),
            'slug' => $request->input('slug'),
            'excerpt' => $request->input('excerpt'),
            'body' => $request->input('body'),
            'category_id' => $request->input('category'),
        ];

        if ($request->hasFile('image')) {
            if (!$request->file('image')->isValid()) {
                return back()->withInput();
            }
            $data['image'] = Post::uploadImage($request->file('image'));
        }

        if (is_null($this->published_at) || $this->published_at > Carbon::now()) {
            $data['published_at'] = $request->published_at;
        }

        $this->update($data);
    }

    public function removeImage()
    {
        if (is_null($this->image)) {
            return false;
        }

        $imageOriginal = $this->dirImg . $this->image;
        $imageThumbnail = $this->dirImg . $this->makeImgThumbName($this->image);

        if (file_exists($imageOriginal)) {
            unlink($imageOriginal);
        }

        if (file_exists($imageThumbnail)) {
            unlink($imageThumbnail);
        }

        return true;
    }

    private function fetchImgName($img)
    {
        return pathinfo($img, PATHINFO_FILENAME);
    }

    private function fetchImgExt($img)
    {
        return pathinfo($img, PATHINFO_EXTENSION);
    }

    private function makeImgThumbName($image)
    {
        // get just file name without extension
        $filename = $this->fetchImgName($image);
        // get extension from filename with extension
        $extension = $this->fetchImgExt($image);

        return $filename . '_thumb.' . $extension;
    }

    public static function index($show, $category)
    {
        $posts = null;
        $trashed = false;

        if ($show == 'published') {
            $posts = static::published()->orderBy('published_at', 'desc');
        } elseif ($show == 'scheduled') {
            $posts = static::scheduled();
        } elseif ($show == 'drafted') {
            $posts = static::drafted();
        } elseif ($show == 'trashed') {
            $posts = static::onlyTrashed()->orderBy('deleted_at', 'desc');
            $trashed = true;
        } elseif ($show == 'category') {
            $posts = static::byCategory($category);
        } else {
            $posts = static::latest();
        }

        return [$posts, $trashed];
    }

    public static function countEveryStatusPosts()
    {
        return [
            'all' => static::all()->count(),
            'published' => static::published()->count(),
            'scheduled' => static::scheduled()->count(),
            'drafted' => static::drafted()->count(),
            'trashed' => static::onlyTrashed()->count(),
        ];
    }
}
