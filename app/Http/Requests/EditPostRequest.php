<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'slug' => 'unique:posts,slug,' . $this->route('blog')->id,
            'body' => 'required',
            'published_at' => 'nullable|date_format:Y/m/d H:i:s',
            'image' => 'mimes:jpeg,jpg,png,bmp'
        ];
    }
}
