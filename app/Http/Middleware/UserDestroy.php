<?php

namespace App\Http\Middleware;

use Closure;

class UserDestroy
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ((int)auth()->id() === (int)$request->route('user')->id) {
            session()->flash('errors', 'You Cannot Remove Yourself');
            return redirect()->route('users.index');
        }else if( (int) auth()->id() === (int) config('cms.user.default') ) {
            return $next($request);
        }

        return abort(403);
    }
}
