<?php

namespace App\Http\Middleware;

use Closure;

class UncategorizedFilter
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if((int) $request->segments()[2] === 6) {
            abort(403);
        }
        return $next($request);
    }
}
