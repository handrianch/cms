<?php

namespace App\Http\Middleware;

use Closure;

class UsersFilterEdit
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ((int)auth()->id() !== (int)config('cms.user.default')
        && (int)auth()->id() !== (int)$request->route('user')->id) {
            abort(403);
        }
        return $next($request);
    }
}
