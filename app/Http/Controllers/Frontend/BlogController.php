<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Post;
use App\Models\User;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::with('author', 'category')
                     ->published()
                     ->latest()
                     ->simplePaginate(3);

        return view('blogs.index')->with('posts', $posts);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        $post->increment('view_count', 1);
        $countPost = $post->author->posts()->published()->count();
        return view('blogs.show')->with('post', $post)
                                 ->with('countPost', $countPost);
    }

    /**
     * Display the specified posts in category.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function category(Category $category)
    {
        $posts = $category->posts()
                          ->with('author')
                          ->published()
                          ->simplePaginate(3);

        return view('blogs.index')->with('posts', $posts)
                                  ->with('categoryName', $category->title);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function author(User $author)
    {
        $posts = $author->posts()
                      ->with('category')
                      ->published()
                      ->simplePaginate(3);

        return view('blogs.index')->with('posts', $posts)
                                  ->with('authorName', $author->name);
    }
}
