<?php

namespace App\Http\Controllers\Backend;

use App\Models\Post;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Requests\EditPostRequest;
use App\Http\Requests\CreatePostRequest;

class BlogController extends BackendController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $show       = strtolower($request->query('show'));
        $category   = strtolower($request->query('category')) ?? null;

        list($posts, $trashed) = Post::index($show, $category);

        $count = $posts->count();

        if($count > 0){
            $posts = $posts->with('category', 'author')->paginate($this->limitPagination);
        }

        $postsCount = Post::countEveryStatusPosts();

        return view('admin.posts.index')->with('posts', $posts)
                                       ->with('postsCount', $postsCount)
                                       ->with('count', $count)
                                       ->with('trashed', $trashed);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::pluck('id', 'title');
        return view('admin.posts.create')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePostRequest $request)
    {
        if ($request->hasFile('image')) {
            $request->image = Post::uploadImage($request->file('image'));
        }

        Post::publish($request);

        return redirect()->route('blog.index')
                        ->with('success', 'Posts success published');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $blog)
    {
        return $blog;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($blog)
    {
        $blog = Post::withTrashed()->findOrFail($blog);
        $categories = Category::pluck('id', 'title');
        return view('admin.posts.edit')->with('post', $blog)
                                     ->with('categories', $categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(EditPostRequest $request, Post $blog)
    {
        $blog->postUpdate($request);
        return redirect()->route('blog.index')->with('success', 'Posts success upadated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $blog)
    {
        $blog->delete();
        return redirect()->back()->with('success', 'Post has been moved to trash')
                                ->with('trashedPost', $blog->id);
    }

    /**
     * Restore the specified resource from storage on trashed
     *
     * @param [type] $blog
     * @return void
     */
    public function restore($blog)
    {
        $blog = Post::onlyTrashed()->findOrFail($blog);
        $blog->restore();
        return redirect()->back()->with('success', 'Post has been restored');
    }

    /**
     * Remove permanently specified resource from storage
     *
     * @param \App\Models\Post $blog
     * @return void
     */
    public function delete($blog)
    {
        $blog = Post::onlyTrashed()->findOrFail($blog);
        $blog->removeImage();
        $blog->forceDelete();

        return redirect()->back()->with('success', 'Post permanently deleted successed');
    }
}
