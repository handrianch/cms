<?php

namespace App\Http\Controllers\Backend;

use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\EditUserRequest;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\DestroyUserRequest;

class UserController extends BackendController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('userFilterEdit')->only(['edit', 'update']);
        $this->middleware('userDestroy')->only(['confirm', 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate($this->limitPagination);
        $count = $users->count();

        return view('admin.users.index')->with('users', $users)
                                       ->with('count', $count);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {
        $data = User::storeRegisterUser($request);
        User::create($data);

        return redirect()->route('users.index')->with('Users success created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('admin.users.edit')->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(EditUserRequest $request, User $user)
    {
        $user->updateUserAccount($request);

        return redirect()->route('users.index')->with('success', 'Users has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyUserRequest $request, User $user)
    {
        $user->deleteUser($request);

        return redirect()->route('users.index')
                        ->with('success', "User {$user->name} success deleted");
    }

    public function confirm(User $user)
    {
        $users      = User::all()->pluck('id', 'name');
        $postCount  = $user->posts()->withTrashed()->count();

        return view('admin.users.confirm-delete')->with('user', $user)
                                                ->with('users', $users)
                                                ->with('postCount', $postCount);
    }
}
