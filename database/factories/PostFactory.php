<?php

use Carbon\Carbon;
use App\Models\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    
    $title = $faker->sentence(mt_rand(6, 12), true);
    $body = $faker->paragraphs(rand(100, 200), true);
    $img = "Post_Image_" . mt_rand(1, 5) . ".jpg";

    $minOrPlus = mt_rand(0, 1) == 1 ? null : '-';
    $dayOrWeek = mt_rand(0, 1) == 1 ? 'days' : 'weeks';
    $count = $dayOrWeek === 'weeks' ? mt_rand(1, 4) : mt_rand(1, 30);
    $date = $minOrPlus ? 
        new Datetime($minOrPlus . $count . ' ' . $dayOrWeek) 
        : 
        new Datetime('-' . mt_rand(1, 12) . 'hours');

    $publishDate = Carbon::instance($date);
    $publishDate = $publishDate->addDays(mt_rand(1, 7));
    $publishDate = $publishDate > Carbon::now() ? $date : $publishDate;

    return [
        'author_id' => $faker->numberBetween(1, 3),
        'category_id' => mt_rand(1, 5),
        'title' => $title,
        'slug' => str_slug($title),
        'excerpt' => mb_substr($body, 0, 120) . '...',
        'body' => $body,
        'image' => mt_rand(0, 1) == 1 ? $img : null,
        'view_count' => mt_rand(1, 10) * 10,
        'created_at' => $date,
        'updated_at' => $date,
        'published_at' => mt_rand(0, 1) == 1 ? $publishDate : null,
    ];
});
